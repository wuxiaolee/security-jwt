package top.surgeqi.security.demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import top.surgeqi.security.demo.bean.po.UserAccount;

/**
 * <p><em>Created by qipp on 2020/7/1 15:36</em></p>
 * 用户账户信息 Mapper 接口
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Repository
public interface UserAccountMapper extends BaseMapper<UserAccount> {

    /**
     * 根据用户名或手机号查询用户账户
     *
     * @param username 用户名或手机号
     * @return org.surge.authorization.model.UserAccount
     * @author qipp
     */
    UserAccount selectByUserName(String username);

    /**
     * 根据手机号查询用户账户
     *
     * @param phone 手机号
     * @return org.surge.authorization.model.UserAccount
     * @author qipp
     */
    UserAccount selectByPhone(String phone);
}
