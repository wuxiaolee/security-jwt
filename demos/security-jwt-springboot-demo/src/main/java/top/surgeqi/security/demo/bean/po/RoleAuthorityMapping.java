package top.surgeqi.security.demo.bean.po;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * <p><em>Created by qipp on 2020/10/1 11:11</em></p>
 * 角色权限映射
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Data
public class RoleAuthorityMapping {
    /**
     * 主键id
     */
    @TableId
    private Long mid;

    /**
     * 角色id
     */
    private Long roleId;

    /**
     * 权限id
     */
    private Long authorityId;
}
