package top.surgeqi.security.demo.config.swagger;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <p><em>Created by qipp on 2020/10/6 4:06 下午</em></p>
 * swagger 配置
 * @author <a href ="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Data
@ConfigurationProperties("surge.swagger")
public class SwaggerProperties {

    /**
     * 文档标题
     */
    private String title;

    /**
     * 描述
     */
    private String description;

    /**
     * 版本
     */
    private String version;

}
