package top.surgeqi.security.demo.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import top.surgeqi.security.demo.bean.core.Result;
import top.surgeqi.security.demo.constants.result.CommonCode;
import top.surgeqi.security.jwt.contants.AuthConstants;
import top.surgeqi.security.jwt.exception.CustomerAuthenticationException;
import top.surgeqi.security.jwt.exception.InvalidTokenException;
import top.surgeqi.security.jwt.exception.TokenExpireException;
import top.surgeqi.security.jwt.handler.AuthenticationExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p><em>Created by qipp on 2020/7/1 14:27</em></p>
 * 自定义通用认证异常处理器
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Component
public class CustomerAuthenticationExceptionHandler implements AuthenticationExceptionHandler {


    /**
     * springmvc启动时自动装配json处理类
     */
    private final ObjectMapper objectMapper;

    public CustomerAuthenticationExceptionHandler(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public void onAuthenticationException(HttpServletRequest request, HttpServletResponse response, CustomerAuthenticationException ex) throws IOException {
        // 设置响应http状态码
        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        // 令牌过期或令牌失效情况返回401
        if(ex instanceof TokenExpireException || ex instanceof InvalidTokenException){
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
        }
        response.setContentType(AuthConstants.CONTENT_TYPE);
        // 返回认证错误与具体认证异常消息
        Result<Object> result = Result.failed(CommonCode.AUTH_ERROR.getResCode(), ex.getMessage());
        response.getWriter().write(objectMapper.writeValueAsString(result));
        response.getWriter().flush();
        response.getWriter().close();
    }
}

