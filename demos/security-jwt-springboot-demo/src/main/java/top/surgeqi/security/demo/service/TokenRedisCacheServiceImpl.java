package top.surgeqi.security.demo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import top.surgeqi.security.jwt.config.JwtProperties;
import top.surgeqi.security.jwt.config.TokenCacheService;

import java.util.concurrent.TimeUnit;

/**
 * <p><em>Created by qipp on 2020/10/6 10:54 上午</em></p>
 * Ehcache 登出令牌缓存操作类
 * 当创建TokenCacheService类型的bean时
 * starter中的 Ehcache bean 以及相关登出令牌缓存操作类将废弃
 * @see top.surgeqi.security.jwt.config.EhCacheConfig
 * <a href ="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Slf4j
@Component
public class TokenRedisCacheServiceImpl implements TokenCacheService {

    /**
     * redis
     */
    private final RedisTemplate<String, String> redisTemplate;
    /**
     * JWT相关配置
     */
    private final JwtProperties jwtProperties;

    public TokenRedisCacheServiceImpl( RedisTemplate<String, String> redisTemplate, JwtProperties jwtProperties) {
        log.info("使用redis缓存存储用户登出令牌");
        this.redisTemplate = redisTemplate;
        this.jwtProperties = jwtProperties;
    }

    @Override
    public void storeLogoutToken(String accessToken, String refreshToken) {
        if (!StringUtils.isEmpty(accessToken)) {
            redisTemplate.opsForValue().set(accessToken,FLAG,jwtProperties.getAccessExpiration(), TimeUnit.MILLISECONDS);
            log.info("用户登出令牌->{}",accessToken);
        }
        if (!StringUtils.isEmpty(refreshToken)) {
            redisTemplate.opsForValue().set(refreshToken,FLAG,jwtProperties.getRefreshExpiration(), TimeUnit.MILLISECONDS);
            log.info("用户登出刷新令牌->{}",refreshToken);
        }
    }

    @Override
    public boolean isLogoutToken(String token) {
        log.info("判断令牌是否被用户登出->{}",token);
        if (!StringUtils.isEmpty(token)) {
            String value = redisTemplate.opsForValue().get(token);
            return !StringUtils.isEmpty(value) && FLAG.equals(value);
        }
        return false;
    }
}
