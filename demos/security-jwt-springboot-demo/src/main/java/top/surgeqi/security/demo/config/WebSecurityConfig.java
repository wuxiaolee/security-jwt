package top.surgeqi.security.demo.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import top.surgeqi.security.demo.handler.CustomAccessDeniedHandler;
import top.surgeqi.security.demo.handler.CustomerAuthenticationSuccessHandler;
import top.surgeqi.security.demo.handler.LoginExpireHandler;
import top.surgeqi.security.demo.handler.LoginFailureHandler;
import top.surgeqi.security.jwt.config.AbstractUserDetailsService;
import top.surgeqi.security.jwt.config.extend.ExtendAuthDaoAuthenticationProvider;
import top.surgeqi.security.jwt.config.extend.ExtendAuthenticationFilter;
import top.surgeqi.security.jwt.contants.AuthConstants;

import java.util.List;
import java.util.Map;

/**
 * <p><em>Created by qipp on 2020/5/29 11:28</em></p>
 * spring security配置
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Configuration
@EnableWebSecurity
@EnableConfigurationProperties(SecurityProperties.class)
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * 角色/权限 分隔符{@value}
     */
    private static final String TO = "2";

    /**
     * 无需授权即可访问的接口{@value}
     */
    private static final String UN_AUTHENTICATED = "unAuthenticated";

    /**
     * 指定角色{@value}
     */
    private static final String ROLE = "role";

    /**
     * 指定权限{@value}
     */
    private static final String AUTH = "auth";


    /**
     * 用户详情Service
     */
    private final AbstractUserDetailsService userDetailsService;

    /**
     * 登录失败处理器
     */
    private final LoginFailureHandler loginFailureHandler;

    /**
     * 登录过期/未登录处理器
     */
    private final LoginExpireHandler loginExpireHandler;

    /**
     * 登录成功处理器
     */
    private final CustomerAuthenticationSuccessHandler authenticationSuccessHandler;

    /**
     * 扩展认证拦截器
     */
    private final ExtendAuthenticationFilter extendAuthenticationFilter;

    /**
     * 权限不足处理器
     */
    private final CustomAccessDeniedHandler customAccessDeniedHandler;

    /**
     * security 验证配置
     */
    private final SecurityProperties securityProperties;

    public WebSecurityConfig(AbstractUserDetailsService userDetailsService,
                             LoginFailureHandler loginFailureHandler,
                             LoginExpireHandler loginExpireHandler,
                             CustomerAuthenticationSuccessHandler authenticationSuccessHandler,
                             ExtendAuthenticationFilter extendAuthenticationFilter,
                             CustomAccessDeniedHandler customAccessDeniedHandler,
                             SecurityProperties securityProperties) {
        this.userDetailsService = userDetailsService;
        this.loginFailureHandler = loginFailureHandler;
        this.loginExpireHandler = loginExpireHandler;
        this.authenticationSuccessHandler = authenticationSuccessHandler;
        this.extendAuthenticationFilter = extendAuthenticationFilter;
        this.customAccessDeniedHandler = customAccessDeniedHandler;
        this.securityProperties = securityProperties;
    }

    /**
     * 配置接口的认证授权
     *
     * @author qipp
     * @param http {@link HttpSecurity}
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        // 配置token验证过滤器
        http.addFilterBefore(extendAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
        // 开启授权认证
        http
                .csrf().disable()
                // rest 无状态 无session
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        // 读取验证配置 设置不受保护路径 设置受保护路径需要的角色权限
        this.readSecurityProperties(http);
        http.authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .anyRequest().authenticated()
                // 登录配置
                .and().formLogin().loginProcessingUrl(AuthConstants.LOGIN_URL)
                // 登录成功后的处理
                .successHandler(authenticationSuccessHandler)
                // 登录失败后的处理
                .failureHandler(loginFailureHandler);

        // 登录过期/未登录 、权限不足 处理
        http.exceptionHandling().authenticationEntryPoint(loginExpireHandler).accessDeniedHandler(customAccessDeniedHandler);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // 覆盖DaoAuthenticationProvider#additionalAuthenticationChecks() 实现（扩展登录不需要验证密码）
        auth.authenticationProvider(new ExtendAuthDaoAuthenticationProvider(userDetailsService));
        auth.userDetailsService(userDetailsService).passwordEncoder(new BCryptPasswordEncoder());
    }

    /**
     * 读取验证配置
     * 设置不受保护路径 设置受保护路径需要的角色权限
     * @param http 请求配置对象
     * @author qipp
     * @throws Exception 获取请求异常
     */
    private void readSecurityProperties(HttpSecurity http) throws Exception {
        // 设置不受保护路径 设置受保护路径需要的角色权限
        Map<String, List<String>> antMatchers = securityProperties.getAntMatchers();
        for (Map.Entry<String, List<String>> entry : antMatchers.entrySet()) {
            String key = entry.getKey();
            List<String> urls = entry.getValue();
            if (key.equals(UN_AUTHENTICATED)) {
                for (String url : urls) {
                    http.authorizeRequests().antMatchers(url).permitAll();
                }
            }else{
                String type = key.split(TO)[0];
                String str = key.split(TO)[1];
                if (type.equals(ROLE)) {
                    for (String url : urls) {
                        http.authorizeRequests().antMatchers(url).hasRole(str);
                    }
                }else if (type.equals(AUTH)) {
                    for (String url : urls) {
                        http.authorizeRequests().antMatchers(url).hasAuthority(str);
                    }
                }
            }
        }
    }
}
