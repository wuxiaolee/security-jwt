package top.surgeqi.security.jwt.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * <p><em>Created by qipp on 2020/2/27 11:28</em></p>
 * 密码编码工具类
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Slf4j
public class BpwdEncoderUtil {

    private static final BCryptPasswordEncoder ENCODER = new BCryptPasswordEncoder();

    /**
     * 用BCryptPasswordEncoder
     *
     * @param password 密码
     * @return java.lang.String 加密后密码
     * @author qipp
     */
    public static String bCryptPassword(String password) {
        return ENCODER.encode(password);
    }

    /**
     * 密码匹配
     *
     * @param rawPassword     未加工密码
     * @param encodedPassword 编码后密码
     * @return boolean        是否匹配
     * @author qipp
     */
    public static boolean matches(CharSequence rawPassword, String encodedPassword) {
        return ENCODER.matches(rawPassword, encodedPassword);
    }

    public static void main(String[] args) {
        String secret = bCryptPassword("123456");
        log.info(secret);
        log.info(""+matches("123456",secret));
    }
}
