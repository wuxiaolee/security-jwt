package top.surgeqi.security.jwt.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * <p><em>Created by qipp on 2020/6/30 12:45</em></p>
 * 无效的Token异常类
 * <p>无效的Token抛出此认证异常</p>
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Slf4j
public class InvalidTokenException extends CustomerAuthenticationException {

    public InvalidTokenException(String msg, Throwable t) {
        super(msg, t);
        log.error("无效的Token异常 msg -> {}, {}", msg, t);
    }

    public InvalidTokenException(String msg) {
        super(msg);
        log.error("无效的Token异常 msg -> {}", msg);
    }
}
