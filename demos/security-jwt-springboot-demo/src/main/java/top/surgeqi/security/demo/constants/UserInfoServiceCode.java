package top.surgeqi.security.demo.constants;

import lombok.Getter;

 /**
 * <p><em>Created by qipp on 2020/10/16 2:15 下午</em></p>
 * 用户信息服务异常枚举
 *
 * @author <a href ="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Getter
public enum UserInfoServiceCode implements ServiceCode{

    /**
     * 用户账户ID已经存在
     */
    USER_ID_ALREADY_EXIST("用户账户ID已经存在", "12000"),

    /**
     * 电话号已经注册过
     */
    PHONE_ALREADY_USE("电话号已经注册过", "12001"),

    /**
     * 用户不存在
     */
    USER_NOT_FOUND("用户不存在", "12002"),

    /**
     * 修改密码 原密码输入错误
     */
    OLD_PWD_ERROR("原密码输入错误", "12003"),

    /**
     * 修改用户关键状态异常
     */
    UPDATE_USER_IMPORTANT_ERROR("修改用户关键状态异常", "12004"),
    ;

    private final String msg;
    private final String code;

    UserInfoServiceCode(String msg, String code) {
        this.msg = msg;
        this.code = code;
    }

}
