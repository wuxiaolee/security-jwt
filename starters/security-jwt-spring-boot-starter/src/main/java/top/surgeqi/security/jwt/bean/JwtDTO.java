package top.surgeqi.security.jwt.bean;

import lombok.Data;

/**
 * <p><em>Created by qipp on 2020/6/2 11:05</em></p>
 * 认证通过或刷新令牌时返回的JWT结构
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Data
public class JwtDTO {
    /**
     * 令牌
     */
    private String accessToken;

    /**
     * 刷新令牌
     */
    private String refreshToken;

    /**
     * 令牌类型
     */
    private String tokenType;

    /**
     * 过期时间
     */
    private Long expiresIn;

    /**
     * JWT ID
     */
    private String jti;
}
