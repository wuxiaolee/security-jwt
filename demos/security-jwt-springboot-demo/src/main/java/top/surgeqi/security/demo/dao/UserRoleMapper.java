package top.surgeqi.security.demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import top.surgeqi.security.demo.bean.po.UserRoleMapping;

/**
 * <p><em>Created by qipp on 2020/7/1 15:36</em></p>
 * 用户角色 Mapper 接口
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Repository
public interface UserRoleMapper extends BaseMapper<UserRoleMapping> {

}
