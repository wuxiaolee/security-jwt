package top.surgeqi.security.demo.constants;

/**
 * <p><em>Created by qipp on 2020/10/16 2:15 下午</em></p>
 * 业务错误枚举接口
 *
 * @author <a href ="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
public interface ServiceCode {

    /**
     * 获取业务错误码
     * @author qipp
     * @return java.lang.String
     */
    String getCode();

    /**
     * 获取业务错误消息
     * @author qipp
     * @return java.lang.String
     */
    String getMsg();
}
