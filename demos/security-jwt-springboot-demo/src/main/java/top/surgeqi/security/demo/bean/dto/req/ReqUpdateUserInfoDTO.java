package top.surgeqi.security.demo.bean.dto.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;

/**
 * <p><em>Created by qipp on 2020/10/15 9:41 上午</em></p>
 * 更新用户详情信息 DTO
 *
 * @author <a href ="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Data
@ApiModel(value = "更新用户详情信息")
public class ReqUpdateUserInfoDTO {

    @NotNull(message = "用户账户Id")
    @ApiModelProperty(value = "用户账户Id")
    private Long accountId;

    @NotNull(message = "id为空")
    @ApiModelProperty(value = "主键")
    private Long id;

    @NotNull(message = "昵称为空")
    @ApiModelProperty(value = "昵称")
    private String nickname;

    @NotNull(message = "性别为空")
    @Pattern(regexp = "([MNF])" ,message = "性别 M:男 F:女")
    @ApiModelProperty(value = "M:男 F:女 N:未知")
    private String sex;

    @NotNull(message = "生日为空")
    @Past
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @ApiModelProperty(value = "生日")
    private LocalDateTime birthday;

    @NotNull(message = "密码为空")
    @ApiModelProperty(value = "头像")
    private String headPortrait;

    @ApiModelProperty(value = "地址")
    private String addressCode;

    @Email
    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "QQ号")
    private String qqNumber;

    @ApiModelProperty(value = "微信号")
    private String wechatNumber;

    @ApiModelProperty(value = "个性签名")
    private String personalizedSignature;

    @ApiModelProperty(value = "是否删除 0：未删除 1：删除")
    private Boolean isDelete;
}
