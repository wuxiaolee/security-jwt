package top.surgeqi.security.demo.bean.dto.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p><em>Created by qipp on 2020/10/16 11:53 上午</em></p>
 * 删除用户 DTO
 *
 * @author <a href ="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Data
@ApiModel(value = "删除用户")
public class ReqDeleteUserInfoDTO {

    @NotNull(message = "用户账户Id")
    @ApiModelProperty(value = "用户账户Id")
    private Long accountId;

    @ApiModelProperty(value = " 是否启用 1:启用 0:未启用")
    private Boolean isEnabled;

    @ApiModelProperty(value = "是否锁定 0:锁定 1:未锁定")
    private Boolean isUnlocked;

    @ApiModelProperty(value = "是否过期 0：过期 1：未过期")
    private Boolean isUnexpired;

    @ApiModelProperty(value = "是否删除 0：未删除 1：删除")
    private Boolean isDelete;
}
