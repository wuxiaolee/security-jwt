package top.surgeqi.security.demo.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import top.surgeqi.security.demo.bean.core.Result;
import top.surgeqi.security.demo.constants.result.CommonCode;
import top.surgeqi.security.jwt.contants.AuthConstants;
import top.surgeqi.security.jwt.contants.ExceptionMsgConstants;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p><em>Created by qipp on 2020/7/1 14:27</em></p>
 * 认证失败处理器
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Slf4j
@Component
public class LoginFailureHandler implements AuthenticationFailureHandler {

    /**
     * springmvc启动时自动装配json处理类
     */
    private final ObjectMapper objectMapper;

    public LoginFailureHandler(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
                                        AuthenticationException exception) throws IOException {
        // 返回认证错误与具体认证异常消息
        Result<Object> rsp = Result.failed(CommonCode.AUTH_ERROR.getResCode(), exception.getMessage());
        log.info("loginFailureHandler ---> {}", rsp);
        // 设置响应http状态码
        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        // 刷新令牌过期或令牌失效情况返回401
        String message = exception.getMessage();
        if(ExceptionMsgConstants.EXPIRE_TOKEN_ERROR_MSG.equals(message) || ExceptionMsgConstants.INVALID_TOKEN_ERROR_MSG.equals(message) ){
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
        }
        response.setContentType(AuthConstants.CONTENT_TYPE);
        response.getWriter().write(objectMapper.writeValueAsString(rsp));
        response.getWriter().flush();
        response.getWriter().close();
    }
}
