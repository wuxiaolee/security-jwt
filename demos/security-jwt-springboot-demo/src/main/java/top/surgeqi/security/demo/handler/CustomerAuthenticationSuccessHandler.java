package top.surgeqi.security.demo.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import top.surgeqi.security.demo.bean.core.Result;
import top.surgeqi.security.jwt.bean.JwtDTO;
import top.surgeqi.security.jwt.config.JwtTokenService;
import top.surgeqi.security.jwt.contants.AuthConstants;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * <p><em>Created by qipp on 2020/7/1 14:27</em></p>
 * 认证成功处理器
 * <blockquote><pre>
 *     自定义认证成功处理器时需要在{@link top.surgeqi.security.demo.config.WebSecurityConfig}
 *     注入认证成功处理器时标注注入bean为{@code  @Qualifier("customerAuthenticationSuccessHandler")}
 * </pre></blockquote>
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Component
public class CustomerAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    /**
     * 令牌相关操作Service
     */
    private final JwtTokenService jwtTokenService;

    /**
     * springmvc启动时自动装配json处理类
     */
    private final ObjectMapper objectMapper;

    public CustomerAuthenticationSuccessHandler(JwtTokenService jwtTokenService, ObjectMapper objectMapper) {
        this.jwtTokenService = jwtTokenService;
        this.objectMapper = objectMapper;
    }

    /**
     * 认证成功触发此接口
     *
     * @param httpServletRequest  请求对象
     * @param httpServletResponse 响应对象
     * @param authentication      认证对象
     * @author qipp
     */
    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        // 生成token
        JwtDTO jwtDTO = jwtTokenService.generateToken(userDetails);
        // 返回令牌对象
        httpServletResponse.setStatus(HttpStatus.OK.value());
        httpServletResponse.setContentType(AuthConstants.CONTENT_TYPE);
        httpServletResponse.getWriter().write(objectMapper.writeValueAsString(Result.succeed(jwtDTO)));
        httpServletResponse.getWriter().flush();
        httpServletResponse.getWriter().close();
    }
}

