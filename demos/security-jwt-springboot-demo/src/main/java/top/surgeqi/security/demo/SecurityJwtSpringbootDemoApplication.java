package top.surgeqi.security.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p><em>Created by qipp on 2020/7/1 10:00</em></p>
 *  security-jwt-starter 使用示例工程
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@MapperScan("top.surgeqi.security.demo.dao")
@SpringBootApplication
public class SecurityJwtSpringbootDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityJwtSpringbootDemoApplication.class, args);
    }

}
