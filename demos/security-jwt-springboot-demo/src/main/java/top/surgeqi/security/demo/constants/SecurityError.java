package top.surgeqi.security.demo.constants;

import lombok.Getter;

/**
 * <p><em>Created by qipp on 2020/7/1 16:53</em></p>
 * 自定义认证异常枚举
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 2.0.1
 */
@Getter
public enum SecurityError implements ServiceCode{

    /**
     * 用户未登录
     */
    USER_NOT_LOGGED_IN("用户未登录", "10000"),

    /**
     * 权限被拒绝
     */
    USER_PERMISSION_DENIED("权限被拒绝", "10001"),

    ;

    /**
     * 授权错误响应消息
     */
    private final String msg;

    /**
     * 授权错误响应码
     */
    private final String code;

    SecurityError(String msg, String code) {
        this.msg = msg;
        this.code = code;
    }
}
