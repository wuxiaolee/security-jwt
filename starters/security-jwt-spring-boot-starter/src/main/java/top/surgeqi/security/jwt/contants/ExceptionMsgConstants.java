package top.surgeqi.security.jwt.contants;

/**
 * <p><em>Created by qipp on 2020/10/9 2:36 下午</em></p>
 * 认证异常消息常量
 * @author <a href ="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
public class ExceptionMsgConstants {

    private ExceptionMsgConstants(){}

    /**
     * 令牌已过期 {@value}
     */
    public static final String EXPIRE_TOKEN_ERROR_MSG = "令牌已过期！";
    /**
     * 无效的令牌 {@value}
     */
    public static final String INVALID_TOKEN_ERROR_MSG = "无效的令牌！";
}
