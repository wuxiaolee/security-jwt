# security-jwt
 > 项目中分为两部分 `starter项目`基于`spring security` 扩展`JWT`与自定义功能 `demo项目`主要为使用者提供使用方法  
 > 适用于前后端分离的单体项目, 微服务环境下推荐使用OAuth2.0项目【[项目地址](https://gitee.com/qipengpai/surge-security-oauth2)】  
 > 希望为正在探索的你的你提供些许的帮助、如果错误还望指出！谢谢！

* [1 启动demo项目](#1)
* [2 项目简介](#2)
* [3 starter使用方法](#3)

# <h2 id="1">1 启动项目</h2>

* demo 项目启动需要环境`JDK1.8` `Mysql 5.6+` `Maven`(安装有问题可以私信)
* 导入项目`security-jwt-springboot-demo`目录下`security-jwt.sql`文件到mysql
* 全局搜索yml配置文件替换 mysql 连接属性
  * mysql配置
  ```yaml
    datasource:
      url: jdbc:mysql://127.0.0.1:3306/security-jwt?characterEncoding=utf8&useSSL=true&serverTimezone=Asia/Shanghai
      password: root
      username: root
  ```
* 体验测试时启动`security-jwt-springboot-demo` demo项目即可

***

# <h2 id="2">2 项目简介</h2>

* 版本`SpringBoot(2.2.2.RELEASE)` 
* 主要基于`spring-boot-starter-security`实现了基于jwt的**认证和授权**、**刷新令牌**、**自由扩展认证方式**
* 代码clone 到本地待完全下载maven依赖后打开,可自行切换分支
  > master 分支为测试过的功能比较稳定  
  > develop 分支包括即将发布的新功能  
  > feature-** 分支为正在最新功能
***
 
# <h2 id="3">3 starter使用方法</h2>
1. 下载源码并安装到本地

    * 下载或clone项目源码
    * 使用 maven 安装到本地即可

2. 在自己的Spring Boot项目里，引入maven依赖
    ```xml
       <dependency>
           <groupId>top.surgeqi</groupId>
           <artifactId>security-jwt-spring-boot-starter</artifactId>
           <version>1.0.1</version>
       </dependency>
    ```
3. 添加配置(application.yml)
     ```yaml
        jwt:
          secret: 123456             # 签名key
          access-expiration: 100000  # 7200000 # 过期时间 毫秒
          refresh-expiration: 200000 # 36000000 # 过期时间 毫秒
          header: Authorization      # 请求头名称 
      ``` 
4. 配置 spring security

 * 同使用spring security项目相同，需要配置`WebSecurityConfig`类！可参照【[示例工程](https://gitee.com/qipengpai/security-jwt/tree/master/demos/security-jwt-springboot-demo)】
 * 实现相应的响应处理器 
    * `AuthenticationEntryPoint` 登录过期/未登录处理器
    * `AccessDeniedHandler` 权限不足处理器
    * `AuthenticationFailureHandler` 认证失败处理器  
    * `AuthenticationSuccessHandler` 认证成功处理器
    * `AuthenticationExceptionHandler` 自定义通用认证异常处理器
 * 继承`AbstractUserDetailsService`抽象类，并实现全部抽象方法
 
5. 认证接口测试
    * 用户名密码
    
    > localhost:8611/auth/login
      ```json
         {
         	"username":"admin",
         	"password":"123",
         	"authType":"password"
         }
      ```   
  * 手机号验证码
  
     > localhost:8611/auth/login
     ```json
        {
        	"phone":"456",
        	"code":"1234",
        	"authType":"sms"
        }
     ```   
  * 微信登陆
    
     > localhost:8611/auth/login
     ```json
        {
        	"unionId":"oouhX0juVT3Vli4evLxU-8NV28D",
        	"authType":"weChat"
        }
     ```       
  * 刷新令牌
    
    > localhost:8611/auth/refresh/token
    ```json
       {
           
           "refreshToken":"eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ7XCJ0b2tlblR5cGVcIjpcInJlZnJlc2hfdG9rZW5cIixcInVzZXJuYW1lXCI6XCJhZG1pblwifSIsImV4cCI6MTU5NzA2ODE4NywiaWF0IjoxNTk3MDY3OTg3NTY3LCJqdGkiOiIyMTY4ZDgzNS1lNGRkLTQ5MWYtYjQ5Mi1iZWJlYmRlY2UwYWMifQ.fV69UO_9wRASWTINFW_iBVVhoPuMXM0Xd7Zd7OuuSDoAno5napLqhKi9T8omKadxmkz7pYdsNCYXocwEFaZWww",
           "authType":"refresh_token"
       }
    ```  

  * 响应结果
    ```json
       {
           "code": "SUCCESS",
           "msg": "请求成功",
           "subCode": "OK",
           "subMsg": "业务请求成功",
           "pagingConditions": null,
           "verificationFailedMsgList": null,
           "data": {
               "accessToken": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ7XCJ0b2tlblR5cGVcIjpcImFjY2Vzc190b2tlblwiLFwidXNlcm5hbWVcIjpcImFkbWluXCJ9IiwiZXhwIjoxNjAxMjY1Njk4LCJpYXQiOjE2MDEyNjU1OTg4NTEsImp0aSI6IjcwMWZhZWIzLWUyY2QtNDFlYi1hOTQwLTVmNjJlMzJlMmZlOCJ9.IG9JSR9kwV0XNHaJYZnovZkYp556JsEcN_F6MEg0IsDSZQzwFwy4SW9wKrFj3WO-l7zj7xGb9MfnwzA4CaDnog",
               "refreshToken": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ7XCJ0b2tlblR5cGVcIjpcInJlZnJlc2hfdG9rZW5cIixcInVzZXJuYW1lXCI6XCJhZG1pblwifSIsImV4cCI6MTYwMTI2NTc5OCwiaWF0IjoxNjAxMjY1NTk4ODUxLCJqdGkiOiI3MDFmYWViMy1lMmNkLTQxZWItYTk0MC01ZjYyZTMyZTJmZTgifQ.hmteO_j1Jsy_qHBy8Zt4kqTxstTNhV5VT5ZGxFmNF6oxoS7rCI9ig0SIkUuadC-3BSWymNDmndY25Tbnu0b2lw",
               "tokenType": "bearer",
               "expiresIn": 100,
               "jti": "701faeb3-e2cd-41eb-a940-5f62e32e2fe8"
           },
           "success": true
       }
    ```

  * 获取用户认证详情接口查看 `UserAccountController` 相关接口介绍了全部获取用户认证详情的接口与权限相关的基本使用
    
    ```java
        @RestController
        public class UserAccountController {
            @GetMapping("/user")
            public Object user4() {
                return SecurityContextHolder.getContext().getAuthentication();
            }
        
            @Secured("ROLE_SURGE_USER")
            @GetMapping("/user1")
            public Object user1(Principal principal) {
                return principal;
            }
        
            @PreAuthorize("hasRole('ROLE_SURGE_ADMIN')")
            @GetMapping("/user2")
            public Object user2(Authentication authentication) {
                return authentication;
            }
        
            @RolesAllowed("ROLE_SURGE_USER")
            @GetMapping("/user3")
            public Object user3(HttpServletRequest request) {
                return request.getUserPrincipal();
            }
        }   
    ```
    > 除开放接口外请求头中需要携带令牌  Authorization    Bearer eyJhbGciOiJIUzUxMiJ9.ey...
    ![请求头中需要携带令牌](https://gitee.com/qipengpai/security-jwt/raw/master/surgeReadmeImage/getUser.jpeg) 

## 如果觉得对您有任何一点帮助，请点右上角 "Star" 支持一下吧，谢谢！
## 如果您对项目感兴趣也欢迎贡献代码！【[Fork + Pull 模式](https://gitee.com/help/articles/4128)】