package top.surgeqi.security.demo.bean.core;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p><em>Created by qipp on 2020/6/28 12:42</em></p>
 * JSR303校验错误显示融合进错误响应
 * <blockquote><pre>
 *     后续提供例子。。。
 * </pre></blockquote>
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Getter
@Setter
@ApiModel("JSR303校验错误显示")
public class VerificationFailedMsg {

    /**
     * 校验错误字段
     */
    @ApiModelProperty("校验错误字段")
    private String field;

    /**
     * 校验错误消息
     */
    @ApiModelProperty("校验错误消息")
    private String errorMessage;
}
