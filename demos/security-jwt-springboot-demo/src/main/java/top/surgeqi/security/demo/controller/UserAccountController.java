package top.surgeqi.security.demo.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import top.surgeqi.security.demo.bean.core.Result;
import top.surgeqi.security.demo.bean.dto.req.*;
import top.surgeqi.security.demo.bean.dto.res.ResUserAccountDTO;
import top.surgeqi.security.demo.service.UserAccountService;
import top.surgeqi.security.demo.service.UserDetailsServiceImpl;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;

/**
 * <p><em>Created by qipp on 2020/6/1 11:11</em></p>
 * 用户账户控制器
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@RestController
@Api(tags = {"用户账户相关"} )
public class UserAccountController {

    /**
     * 用户账户service
     */
    private final UserDetailsServiceImpl userDetailsService;
    /**
     * 用户账户服务类
     */
    private final UserAccountService userAccountService;

    public UserAccountController(UserDetailsServiceImpl userDetailsService, UserAccountService userAccountService) {
        this.userDetailsService = userDetailsService;
        this.userAccountService = userAccountService;
    }

    /**
     * 获取用户认证对象
     * @return java.lang.Object
     * @author qipp
     */
    @GetMapping("/user")
    @ApiOperation(value = "获取用户账户信息")
    public Result<ResUserAccountDTO> getUser() {
        return userAccountService.selectUserAccountInfo();
    }

    /**
     * 用户登出
     * @author qipp
     * @param reqUserLogoutDTO 用户登出DTO
     * @return top.surgeqi.security.demo.bean.result.Result<java.lang.Object>
     */
    @PostMapping("/user/logout")
    @ApiOperation(value = "用户登出")
    public Result<Object> userLogout(@RequestBody ReqUserLogoutDTO reqUserLogoutDTO) {
        userDetailsService.storeLogoutToken(reqUserLogoutDTO.getAccessToken(),reqUserLogoutDTO.getRefreshToken());
        return Result.succeed();
    }

    /**
     * 用户注册
     * @author qipp
     * @param reqUserAccountInitializeDTO 用户注册 DTO
     * @return top.surgeqi.security.demo.bean.result.Result<java.lang.Object>
     */
    @PostMapping("/unAuthenticated/user")
    @ApiOperation(value = "用户注册")
    public Result<Object> initializeUser(@Valid @RequestBody ReqUserAccountInitializeDTO reqUserAccountInitializeDTO) {
        return userAccountService.initializeUser(reqUserAccountInitializeDTO);
    }

    /**
     * 用户忘记密码
     * @author qipp
     * @param reqForgetPwdDTO 用户忘记密码 DTO
     * @return top.surgeqi.security.demo.bean.result.Result<java.lang.Object>
     */
    @PutMapping(value = "/unAuthenticated/forgetPwd")
    @ApiOperation(value = "用户忘记密码")
    public Result<Object> forgetPwd(@Valid @RequestBody ReqForgetPwdDTO reqForgetPwdDTO) {
        return userAccountService.forgetPwd(reqForgetPwdDTO);
    }

    /**
     * 更新用户密码
     * @author qipp
     * @param reqForgetPwdDTO 更新用户密码 DTO
     * @return top.surgeqi.security.demo.bean.result.Result<java.lang.Object>
     */
    @PutMapping(value = "/updatePwd")
    @ApiOperation(value = "更新用户密码")
    public Result<Object> updatePwd(@Valid @RequestBody ReqUpdatePwdDTO reqForgetPwdDTO) {
        return userAccountService.updatePwd(reqForgetPwdDTO);
    }


    /**
     * 删除用户
     * @author qipp
     * @param reqDeleteUserInfoDTO 用户账户ID
     * @return top.surgeqi.security.demo.bean.result.Result<java.lang.Object>
     */
    @DeleteMapping("/user")
    @ApiOperation(value = "删除用户")
    public Result<Object> deleteUser(@Valid @RequestBody ReqDeleteUserInfoDTO reqDeleteUserInfoDTO) {
        return userAccountService.deleteUser(reqDeleteUserInfoDTO);
    }

    @Secured("ROLE_SURGE_USER")
    @GetMapping("/user1")
    public Object user1(Principal principal) {
        return principal;
    }

    @PreAuthorize("hasRole('ROLE_SURGE_ADMIN')")
    @GetMapping("/user2")
    public Object user2(Authentication authentication) {
        return authentication;
    }

    @RolesAllowed("ROLE_SURGE_USER")
    @GetMapping("/user3")
    public Object user3(HttpServletRequest request) {
        return request.getUserPrincipal();
    }
}
