package top.surgeqi.security.jwt.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <p><em>Created by qipp on 2020/6/30 12:54</em></p>
 * JWT相关配置
 * <blockquote><pre>
 *     jwt配置提供默认值
 *     secret: 123456
 *     accessExpiration: 7200000
 *     refreshExpiration: 36000000
 *     header: Authorization
 * </pre></blockquote>
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 2.0.1
 */
@Data
@ConfigurationProperties(value = "jwt")
public class JwtProperties {

    /**
     * 密钥
     */
    private String secret = "123456";

    /**
     * 令牌过期时间 毫秒
     */
    private Long accessExpiration = 7200000L;

    /**
     * 刷新令牌过期时间 毫秒
     */
    private Long refreshExpiration = 36000000L;

    /**
     * 请求头
     */
    private String header = "Authorization";

    /**
     * 失效令牌缓存name
     */
    private String logoutAccessTokenName = "logoutAccessToken";

    /**
     * 失效刷新令牌缓存name
     */
    private String logoutRefreshTokenName = "logoutRefreshToken";

}
