package top.surgeqi.security.demo.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Snowflake;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.surgeqi.security.demo.bean.core.Result;
import top.surgeqi.security.demo.bean.dto.req.ReqDeleteUserInfoDTO;
import top.surgeqi.security.demo.bean.dto.req.ReqForgetPwdDTO;
import top.surgeqi.security.demo.bean.dto.req.ReqUpdatePwdDTO;
import top.surgeqi.security.demo.bean.dto.req.ReqUserAccountInitializeDTO;
import top.surgeqi.security.demo.bean.dto.res.ResUserAccountDTO;
import top.surgeqi.security.demo.bean.po.*;
import top.surgeqi.security.demo.constants.UserInfoServiceCode;
import top.surgeqi.security.demo.dao.*;
import top.surgeqi.security.demo.service.UserAccountService;
import top.surgeqi.security.demo.service.UserInfoService;
import top.surgeqi.security.jwt.utils.BpwdEncoderUtil;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p><em>Created by qipp on 2020/2/27 14:36</em></p>
 * 用户账户ServiceImpl
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Slf4j
@Service
public class UserAccountServiceImpl implements UserAccountService {

    /**
     * 用户账户Mapper
     */
    private final UserAccountMapper userAccountMapper;

    /**
     * 用户角色映射Mapper
     */
    private final UserRoleMappingMapper userRoleMappingMapper;

    /**
     * 用户绑定Mapper
     */
    private final UserBindingMapper userBindingMapper;

    /**
     * 用户角色Mapper
     */
    private final RoleMapper roleMapper;

    /**
     * 用户权限Mapper
     */
    private final AuthorityMapper authorityMapper;

    /**
     * 用户信息Mapper
     */
    private final UserInfoService userInfoService;

    /**
     * 雪花算法
     */
    private final Snowflake snowflake;

    public UserAccountServiceImpl(UserAccountMapper userAccountMapper,
                                  UserRoleMappingMapper userRoleMappingMapper,
                                  UserBindingMapper userBindingMapper,
                                  RoleMapper roleMapper,
                                  AuthorityMapper authorityMapper,
                                  UserInfoService userInfoService,
                                  Snowflake snowflake) {
        this.userAccountMapper = userAccountMapper;
        this.userRoleMappingMapper = userRoleMappingMapper;
        this.roleMapper = roleMapper;
        this.authorityMapper = authorityMapper;
        this.userBindingMapper = userBindingMapper;
        this.userInfoService = userInfoService;
        this.snowflake = snowflake;
    }

    /**
     * 根据用户名或手机号查询用户账户
     * {@inheritDoc}
     */
    @Override
    public UserAccount selectByUserName(String username) {
        // 根据用户名查询用户账户
        return userAccountMapper.selectByUserName(username);
    }

    /**
     * 根据手机号查询用户账户
     * {@inheritDoc}
     */
    @Override
    public UserAccount selectByPhone(String phone) {
        // 根据手机号查询用户账户
        return userAccountMapper.selectByPhone(phone);
    }

    /**
     * 查询绑定微信ID的用户账户信息
     * {@inheritDoc}
     */
    @Override
    public UserAccount selectWeChatUnionId(String unionId) {

        // 获取绑定微信的用户ID
        UserBinding userBinding = userBindingMapper.selectWeChatUnionId(unionId);
        // 微信未绑定用户
        if (null == userBinding) {
            log.error("微信未绑定用户 unionId -> {}", unionId);
            return null;
        }
        // 查询用户账户信息
        return userAccountMapper.selectById(userBinding.getAccountId());
    }

    @Override
    public Result<ResUserAccountDTO> selectUserAccountInfo() {
        //获取上下文中用户详情
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserAccount userAccount = (UserAccount) authentication.getPrincipal();
        ResUserAccountDTO resUserAccountDTO = new ResUserAccountDTO();
        // 对象拷贝（偷懒了 - -）
        BeanUtil.copyProperties(userAccount, resUserAccountDTO);
        // 查询数据库中用户信息
        UserInfo userInfo = userInfoService.getUserInfo(userAccount.getMid());
        // 昵称
        resUserAccountDTO.setNickname(userInfo.getNickname());
        // 头像
        resUserAccountDTO.setHeadPortrait(userInfo.getHeadPortrait());
        // 签名
        resUserAccountDTO.setPersonalizedSignature(userInfo.getPersonalizedSignature());
        // 性别
        resUserAccountDTO.setSex(userInfo.getSex());
        // 返回用户DTO
        return Result.succeed(resUserAccountDTO);
    }

    /**
     * 创建用户账户对象，创建用户详情对象
     *
     * @author qipp
     * {@inheritDoc
     */
    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Result<Object> initializeUser(ReqUserAccountInitializeDTO reqUserAccountInitializeDTO) {

        // todo 验证码校验（验证码api对接发送）
        String phoneCode = reqUserAccountInitializeDTO.getPhoneCode();
        log.trace("注册验证码 -> {}", phoneCode);
        // 用户新增对象
        UserAccount userAccount = new UserAccount();
        // 雪花算法生成用户ID
        Long accountId = snowflake.nextId();
        userAccount.setMid(accountId);
        // 用户账号
        userAccount.setUsername(reqUserAccountInitializeDTO.getUsername());
        // 密码
        userAccount.setPassword(BpwdEncoderUtil.bCryptPassword(reqUserAccountInitializeDTO.getPassword()));
        // 手机号
        userAccount.setPhone(reqUserAccountInitializeDTO.getPhone());
        // 注册时间
        final LocalDateTime now = LocalDateTime.now();
        userAccount.setCreateTime(now);
        userAccount.setUpdateTime(now);
        // 创建用户账号
        userAccountMapper.insert(userAccount);

        // 赋予用户普通用户角色
        UserRoleMapping userRoleMapping = new UserRoleMapping();
        userRoleMapping.setMid(snowflake.nextId());
        userRoleMapping.setAccountId(accountId);
        userRoleMapping.setRoleId(3L);
        userRoleMappingMapper.insert(userRoleMapping);
        return userInfoService.initialize(accountId, reqUserAccountInitializeDTO.getNickname());
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Result<Object> forgetPwd(ReqForgetPwdDTO reqForgetPwdDTO) {
        // todo 验证码校验（验证码api对接发送）
        String phoneCode = reqForgetPwdDTO.getPhoneCode();
        log.trace("忘记密码验证码 -> {}", phoneCode);
        UserAccount userAccount = userAccountMapper.selectByPhone(reqForgetPwdDTO.getPhone());
        if (null == userAccount) {
            return Result.failed(UserInfoServiceCode.USER_NOT_FOUND);
        }
        UserAccount updateUser = new UserAccount();
        updateUser.setMid(userAccount.getMid());
        updateUser.setPassword(BpwdEncoderUtil.bCryptPassword(reqForgetPwdDTO.getPassword()));
        userAccountMapper.updateById(updateUser);
        log.trace("找回密码 accountId -> {}", userAccount.getMid());
        return Result.succeed();
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Result<Object> updatePwd(ReqUpdatePwdDTO reqForgetPwdDTO) {
        //获取上下文中用户详情
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserAccount userAccount = (UserAccount) authentication.getPrincipal();
        // 验证原密码是否正确
        if (!BpwdEncoderUtil.matches(reqForgetPwdDTO.getPassword(), userAccount.getPassword())) {
            return Result.failed(UserInfoServiceCode.OLD_PWD_ERROR);
        }
        // 更新用户密码
        UserAccount updateUser = new UserAccount();
        updateUser.setMid(userAccount.getMid());
        updateUser.setPassword(BpwdEncoderUtil.bCryptPassword(reqForgetPwdDTO.getNewPassword()));
        userAccountMapper.updateById(updateUser);
        log.trace("更新用户密码 accountId -> {}", userAccount.getMid());
        return Result.succeed();
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Result<Object> deleteUser(ReqDeleteUserInfoDTO reqDeleteUserInfoDTO) {
        // 用户账户ID
        Long accountId = reqDeleteUserInfoDTO.getAccountId();
        // 当前时间
        LocalDateTime now = LocalDateTime.now();
        // 更新用户密码
        UserAccount updateUser = new UserAccount();
        updateUser.setMid(accountId);
        if (null != reqDeleteUserInfoDTO.getIsDelete()) {
            log.info("逻辑删除用户账户 accountId -> {}", accountId);
            updateUser.setIsDelete(reqDeleteUserInfoDTO.getIsDelete());
            updateUser.setUpdateTime(now);
            userAccountMapper.updateById(updateUser);
            UserInfo userInfo = new UserInfo();
            userInfo.setAccountId(accountId);
            userInfo.setIsDelete(reqDeleteUserInfoDTO.getIsDelete());
            userInfo.setUpdateTime(now);
            userInfoService.logicDeleteUser(userInfo);
            return Result.succeed();
        }
        // todo  锁定 禁用用户
        return Result.failed(UserInfoServiceCode.UPDATE_USER_IMPORTANT_ERROR);
    }

    /**
     * 组合用户账户对象与角色权限
     * {@inheritDoc}
     */
    @Override
    public UserAccount composeUserDetailsAndAuthority(UserAccount userAccount) {
        // 查询用户角色
        List<Role> roleList = roleMapper.selByUserId(userAccount.getMid());
        log.debug("获取启用角色集合{}", roleList);
        // 角色权限集合
        List<GrantedAuthority> authorities = new ArrayList<>(roleList);
        // 权限临时集合
        List<Authority> radAuthoritiesList = new ArrayList<>();
        for (Role role : roleList) {
            // 根据角色id查询权限集合
            List<Authority> radAuthorities = authorityMapper.selByRoleId(role.getMid()).stream().distinct().collect(Collectors.toList());
            log.info("权限集合（去重后的）{}", radAuthorities);
            boolean j = radAuthoritiesList.addAll(radAuthorities);
            log.debug("拼接权限集合{}，{}", j, radAuthoritiesList);
        }
        authorities.addAll(radAuthoritiesList);
        log.info("拼接角色和权限后的oauth权限结合{}", authorities);
        // 设置用户角色权限集合
        userAccount.setAuthorities(authorities);
        return userAccount;
    }
}
