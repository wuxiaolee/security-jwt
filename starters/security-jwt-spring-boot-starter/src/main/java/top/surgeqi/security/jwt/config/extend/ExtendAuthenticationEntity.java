package top.surgeqi.security.jwt.config.extend;

import lombok.Data;

import java.util.Map;

/**
 * <p><em>Created by qipp on 2020/2/27 10:21</em></p>
 * 扩展认证上下文实体
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Data
public class ExtendAuthenticationEntity {

    /**
     * 获取token扩展授权类型
     */
    private String authType;

    /**
     * 请求登录认证参数集合
     */
    private Map<String, String[]> authParameters;

    /**
     * 获取参数中的值
     *
     * @param parameter 请求参数
     * @return java.lang.String
     * @author qipp
     */
    public String getAuthParameter(String parameter) {
        String[] values = this.authParameters.get(parameter);
        if (values != null && values.length > 0) {
            return values[0];
        }
        return null;
    }
}
