package top.surgeqi.security.jwt.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * <p><em>Created by qipp on 2020/6/30 12:45</em></p>
 * Token过期异常类
 * <p>Token过期抛出此认证异常</p>
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Slf4j
public class TokenExpireException extends CustomerAuthenticationException {

    public TokenExpireException(String msg, Throwable t) {
        super(msg, t);
        log.error("Token过期异常 msg -> {}", msg, t);
    }

    public TokenExpireException(String msg) {
        super(msg);
        log.error("Token过期异常 msg -> {}", msg);
    }
}
