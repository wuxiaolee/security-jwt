package top.surgeqi.security.demo.bean.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.Collection;

/**
 * <p><em>Created by qipp on 2020/10/1 11:11</em></p>
 * 用户账户
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Data
public class UserAccount implements UserDetails {
    /**
     * 主键id
     */
    @TableId
    private Long mid;

    /**
     * 魔咕号，只能设置一次
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 电话号
     */
    private String phone;

    /**
     * 是否启用 1:启用 0:未启用
     */
    private Boolean isEnabled;

    /**
     * 是否锁定 0:锁定 1:未锁定
     */
    private Boolean isUnlocked;

    /**
     * 是否过期 0：过期 1：未过期
     */
    @TableField("is_unExpired")
    private Boolean isUnexpired;

    /**
     * 是否删除 0：未删除 1：删除
     */
    private Boolean isDelete;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    private transient Collection<? extends GrantedAuthority> authorities;

    @Override
    public boolean isAccountNonExpired() {
        return this.isUnexpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.isUnlocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.isEnabled;
    }
}
