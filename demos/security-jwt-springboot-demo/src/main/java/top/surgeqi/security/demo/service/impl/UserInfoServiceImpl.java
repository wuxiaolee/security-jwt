package top.surgeqi.security.demo.service.impl;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import top.surgeqi.security.demo.bean.core.ReqPage;
import top.surgeqi.security.demo.bean.core.ResPage;
import top.surgeqi.security.demo.bean.core.Result;
import top.surgeqi.security.demo.bean.dto.req.ReqUpdateUserInfoDTO;
import top.surgeqi.security.demo.bean.dto.res.ResGetUserInfoDTO;
import top.surgeqi.security.demo.bean.po.UserAccount;
import top.surgeqi.security.demo.bean.po.UserInfo;
import top.surgeqi.security.demo.constants.UserInfoServiceCode;
import top.surgeqi.security.demo.dao.UserAccountMapper;
import top.surgeqi.security.demo.dao.UserInfoMapper;
import top.surgeqi.security.demo.service.UserInfoService;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;


/**
 * 用户信息服务实现类
 * <blockquote><pre>
 *      mybatis plus wrapper 使用方法
 *      查询方式	        说明
 *      setSqlSelect	设置 SELECT 查询字段
 *      where	        WHERE 语句，拼接 + WHERE 条件
 *      and	            AND 语句，拼接 + AND 字段=值
 *      andNew	        AND 语句，拼接 + AND (字段=值)
 *      or	            OR 语句，拼接 + OR 字段=值
 *      orNew	        OR 语句，拼接 + OR (字段=值)
 *      eq	            等于=
 *      allEq	        基于 map 内容等于=
 *      ne	            不等于<>
 *      gt	            大于>
 *      ge	            大于等于>=
 *      lt	            小于<
 *      le	            小于等于<=
 *      like	            模糊查询 LIKE
 *      notLike	        模糊查询 NOT LIKE
 *      in	            IN 查询
 *      notIn	        NOT IN 查询
 *      isNull	        NULL 值查询
 *      isNotNull	    IS NOT NULL
 *      groupBy	        分组 GROUP BY
 *      having	        HAVING 关键词
 *      orderBy	        排序 ORDER BY
 *      orderAsc	        ASC 排序 ORDER BY
 *      orderDesc	    DESC 排序 ORDER BY
 *      exists	        EXISTS 条件语句
 *      notExists	    NOT EXISTS 条件语句
 *      between	        BETWEEN 条件语句
 *      notBetween	    NOT BETWEEN 条件语句
 *      addFilter	    自由拼接 SQL
 *      last	            拼接在最后，例如：last("LIMIT 1")
 * </pre></blockquote>
 *
 * @author qipp
 * @date 2020-05-15
 * @since 1.0.1
 */
@Slf4j
@Service
public class UserInfoServiceImpl implements UserInfoService {

    /**
     * 用户信息Mapper
     */
    @Resource
    private UserInfoMapper userInfoMapper;

    /**
     * 用户账户Mapper
     */
    @Resource
    private UserAccountMapper userAccountMapper;

    /**
     * 雪花算法
     */
    @Resource
    private Snowflake snowflake;

    /**
     * 账户ID
     */
    private static final String ACCOUNT_ID = "account_id";

    /**
     * 初始化用户信息详情适当填充默认值
     * {@inheritDoc}
     *
     * @author qipp
     * @date 2020/5/15 12:00
     */
    @Override
    public Result<Object> initialize(Long accountId, String nickname) {
        log.info("初始化用户信息 accountId -> {} ", accountId);
        // 如果该账户已初始化，则返回该记录id
        QueryWrapper<UserInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(ACCOUNT_ID, accountId);
        UserInfo existing = userInfoMapper.selectOne(queryWrapper);
        // 账户ID重复返回错误
        if (null != existing) {
            return Result.failed(UserInfoServiceCode.USER_ID_ALREADY_EXIST);
        }

        // 1.昵称初始化形式为：昵称+6位随机数。 注：昵称可以重复
        // 2.性别默认初始值为F女
        UserInfo userInfo = new UserInfo();
        // 主键
        userInfo.setId(snowflake.nextId());
        // 用户账户id
        userInfo.setAccountId(accountId);
        // 昵称  微信游客注册用户的时候昵称性别头像需要带过来
        userInfo.setNickname(StringUtils.isNotEmpty(nickname) ? nickname : "昵称" + RandomUtil.randomInt(100000, 999999));
        // 性别
        userInfo.setSex("F");
        // 默认头像
        userInfo.setHeadPortrait("https://cube.elemecdn.com/0/88/03b0d39583f48206768a7534e55bcpng.png");
        // 注册时间
        final LocalDateTime now = LocalDateTime.now();
        userInfo.setCreateTime(now);
        userInfo.setUpdateTime(now);
        // 数据库新增用户详情
        userInfoMapper.insert(userInfo);
        return Result.succeed();
    }

    @Override
    public Result<ResGetUserInfoDTO> getUserInfoDTO(Long accountId) {
        // 用户详情信息
        UserInfo userInfo = this.getUserInfo(accountId);
        return Result.succeed(makeUserInfoToResGetUserInfoDTO(userInfo, new ResGetUserInfoDTO()));
    }

    /**
     * 用户详情对象转用户详情DTO
     *
     * @param userInfo 用户详情对象
     * @return top.surgeqi.security.demo.bean.dto.res.ResGetUserInfoDTO
     * @author qipp
     */
    private ResGetUserInfoDTO makeUserInfoToResGetUserInfoDTO(UserInfo userInfo, ResGetUserInfoDTO resGetUserInfoDTO) {
        // 返回用户信息DTO
        resGetUserInfoDTO.setAccountId(userInfo.getAccountId());
        resGetUserInfoDTO.setId(userInfo.getId());
        resGetUserInfoDTO.setNickname(userInfo.getNickname());
        resGetUserInfoDTO.setSex(userInfo.getSex());
        resGetUserInfoDTO.setBirthday(userInfo.getBirthday());
        resGetUserInfoDTO.setHeadPortrait(userInfo.getHeadPortrait());
        resGetUserInfoDTO.setAddressCode(userInfo.getAddressCode());
        resGetUserInfoDTO.setEmail(userInfo.getEmail());
        resGetUserInfoDTO.setQqNumber(userInfo.getQqNumber());
        resGetUserInfoDTO.setWechatNumber(userInfo.getWechatNumber());
        resGetUserInfoDTO.setPersonalizedSignature(userInfo.getPersonalizedSignature());
        resGetUserInfoDTO.setIsDelete(userInfo.getIsDelete());
        resGetUserInfoDTO.setCreateTime(userInfo.getCreateTime());
        resGetUserInfoDTO.setUpdateTime(userInfo.getUpdateTime());
        return resGetUserInfoDTO;
    }

    @Override
    public UserInfo getUserInfo(Long accountId) {
        log.info("查询用户信息用户id -> {}", accountId);
        QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();
        wrapper.eq(ACCOUNT_ID, accountId);
        // 查询数据库中用户信息
        return userInfoMapper.selectOne(wrapper);
    }

    @Override
    public Result<Object> updateUserInfo(ReqUpdateUserInfoDTO reqUpdateUserInfoDTO) {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(reqUpdateUserInfoDTO.getId());
        userInfo.setAddressCode(reqUpdateUserInfoDTO.getAddressCode());
        userInfo.setHeadPortrait(reqUpdateUserInfoDTO.getHeadPortrait());
        userInfo.setBirthday(reqUpdateUserInfoDTO.getBirthday());
        userInfo.setNickname(reqUpdateUserInfoDTO.getNickname());
        userInfo.setQqNumber(reqUpdateUserInfoDTO.getQqNumber());
        userInfo.setWechatNumber(reqUpdateUserInfoDTO.getWechatNumber());
        userInfo.setEmail(reqUpdateUserInfoDTO.getEmail());
        userInfo.setSex(reqUpdateUserInfoDTO.getSex());
        userInfo.setPersonalizedSignature(reqUpdateUserInfoDTO.getPersonalizedSignature());
        userInfo.setUpdateTime(LocalDateTime.now());
        userInfoMapper.updateById(userInfo);
        return Result.succeed();
    }

    @Override
    public Result<Object> logicDeleteUser(UserInfo userInfo) {
        Long accountId = userInfo.getAccountId();
        if (null == accountId) {
            return Result.failed(UserInfoServiceCode.USER_NOT_FOUND);
        }
        log.info("逻辑删除用户详情信息 accountId -> {}", accountId);
        UpdateWrapper<UserInfo> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq(ACCOUNT_ID,accountId);
        userInfoMapper.update(userInfo, updateWrapper);
        return Result.succeed();
    }

    @Override
    public Result<ResPage<ResGetUserInfoDTO, LocalDateTime>> getUserInfoList(ReqPage<ReqUpdateUserInfoDTO, LocalDateTime> reqUpdateUserInfoDTO) {
        // 分页参数
        Page<UserAccount> page = new Page<>(reqUpdateUserInfoDTO.getPage(), reqUpdateUserInfoDTO.getRows());
        // 分页条件
        QueryWrapper<UserAccount> wrapper = new QueryWrapper<>();
        // 分页标识位
        LocalDateTime flag = null != reqUpdateUserInfoDTO.getFlag() ? reqUpdateUserInfoDTO.getFlag() : LocalDateTime.now();
        wrapper.le("create_time", flag);
        wrapper.eq("is_enabled", 1);
        wrapper.eq("is_unlocked", 1);
        wrapper.eq("is_unExpired", 1);
        wrapper.eq("is_delete", 0);
        // 查询结果
        Page<UserAccount> userAccountPage = userAccountMapper.selectPage(page, wrapper);
        log.info(userAccountPage.toString());
        // 返回结果
        ResPage<ResGetUserInfoDTO, LocalDateTime> resPage = new ResPage<>();
        resPage.setPage(userAccountPage.getCurrent());
        resPage.setRows(userAccountPage.getSize());
        resPage.setTotal(userAccountPage.getTotal());
        resPage.setFlag(flag);
        if(CollectionUtils.isEmpty(userAccountPage.getRecords())){
            return Result.succeed(resPage);
        }

        // 拼装返回结果
        List<ResGetUserInfoDTO> collect = userAccountPage.getRecords().stream().map(userAccount -> {
            ResGetUserInfoDTO resGetUserInfoDTO = new ResGetUserInfoDTO();
            resGetUserInfoDTO.setAccountId(userAccount.getMid());
            resGetUserInfoDTO.setPhone(userAccount.getPhone());
            resGetUserInfoDTO.setUsername(userAccount.getUsername());
            return resGetUserInfoDTO;
        }).collect(Collectors.toList());

        // 查询用户详情（不连表查了微服务的都得拆开拼装）（查询非用户账户属性只允许等量查询）
        QueryWrapper<UserInfo> userInfoQueryWrapper = new QueryWrapper<>();
        userInfoQueryWrapper.in(ACCOUNT_ID, collect.stream().map(ResGetUserInfoDTO::getAccountId).collect(Collectors.toList()));
        List<UserInfo> userInfos = userInfoMapper.selectList(userInfoQueryWrapper);
        collect.forEach(resGetUserInfoDTO -> userInfos.stream()
                .filter(userInfo -> userInfo.getAccountId().equals(resGetUserInfoDTO.getAccountId()))
                .findFirst()
                .ifPresent(userInfo -> makeUserInfoToResGetUserInfoDTO(userInfo, resGetUserInfoDTO)));

        resPage.setList(collect);
        return Result.succeed(resPage);
    }
}
