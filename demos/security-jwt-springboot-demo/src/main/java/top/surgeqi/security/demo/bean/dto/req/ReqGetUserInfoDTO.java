package top.surgeqi.security.demo.bean.dto.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p><em>Created by qipp on 2020/10/9 9:41 上午</em></p>
 * 获取用户列表 DTO
 *
 * @author <a href ="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Data
@ApiModel(value = "获取用户列表")
public class ReqGetUserInfoDTO {

    @ApiModelProperty(value = "用户账户Id")
    private String accountId;

}
